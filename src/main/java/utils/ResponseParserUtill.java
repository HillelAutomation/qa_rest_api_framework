package utils;

import com.google.gson.Gson;

import java.util.List;

public class ResponseParserUtill {

    private static Gson gson = new Gson();

    public static List<String> fromJson(final String json) {
        return gson.fromJson(json, List.class);
    }
}
