package api;

import utils.ResponseParserUtill;

import java.util.List;

public class JsonPlaceholderApi extends DefaultApiConfiguration {

    private static final String POSTS_PARTIAL_PATH = "/posts";
    private static final String POSTS_COMMENTS_PATH = "/comments";
    private static final String POSTS_ALBUMS_PATH = "/albums";
    private static final String POSTS_PHOTOS_PATH = "/photos";
    private static final String POSTS_TODOS_PATH = "/todos";
    private static final String POSTS_USERS_PATH = "/users";

    public List<String> getAlbums() {
        final String response = getWebResource()
                .path(POSTS_ALBUMS_PATH)
                .get(String.class);
        return ResponseParserUtill.fromJson(response);
    }

    public List<String> getComments() {
        return null;
    }

    public List<String> getPhotos() {
        return null;
    }

    public List<String> getTodos() {
        return null;
    }

    public List<String> getUsers() {
        return null;
    }

    public List<String> getPosts() {
        return null;
    }
}
