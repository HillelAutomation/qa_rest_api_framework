package api;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

public abstract class DefaultApiConfiguration {

    private Client client;
    private WebResource webResource;

    private static final String API_BASE_PATH = "https://jsonplaceholder.typicode.com/";

    protected DefaultApiConfiguration() {
        createDefaultClient();
    }

    public WebResource getWebResource() {
        return webResource;
    }

    private void createDefaultClient() {
        client = Client.create();
        webResource = client.resource(API_BASE_PATH);
    }
}
